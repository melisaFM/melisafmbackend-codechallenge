<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;

class DriverController extends Controller
{
    /**
     * @Route("/driver/index", name="app_driver_index")
     */
    public function indexAction(Request $request)
    {
        $driver = $this->getDoctrine()->getRepository('AppBundle:Driver')->findAll();
        return $this->render('driver/index.html.twig', ['driver' => $driver]);
    }

    /**
     * @Route("/driver/new")
     */
    public function newAction(Request $request)
    {
        $driver = new DriverForm();
        $form = $this->createFormBuilder($driver)
            ->add('name', TextType::class)
            ->add('created', DateType::class)
            ->add('updated', DateType::class)
            ->add('status', ChoiceType::class)
            ->add('save', SubmitType::class, ['label' => 'Submit'])
            ->getForm();

        $form->handleRequest($request);
   
        if ($form->isSubmitted() && $form->isValid()) {
            $driver= $form->getData();
            $doct = $this->getDoctrine()->getManager();
            $doct->persist($driver;
            $doct->flush();
            return $this->redirectToRoute('app_driver_index');
        } else {
            return $this->render('driver/new.html.twig', ['form' =>$form->createView()]);
        }
    }

    /** 
     * @Route("/driver/update/{id}", name = "app_driver_update" ) 
     */
    public function updateAction($id, Request $request) {
        $doct = $this->getDoctrine()->getManager();
        $driver = $doct->getRepository('AppBundle:Driver')->find($id);
        
        if (!$driver) {
            throw $this->createNotFoundException('No existe el driver con id: '.$id);
        }  
        $form = $this->createFormBuilder($driver)
            ->add('name', TextType::class)
            ->add('created', DateType::class)
            ->add('updated', DateType::class)
            ->add('status', ChoiceType::class)
            ->add('save', SubmitType::class, ['label' => 'Submit'])
            ->getForm();

        $form->handleRequest($request);
       
        if ($form->isSubmitted() && $form->isValid()) {
            $driver_info = $form->getData();
            $doct = $this->getDoctrine()->getManager();
            $doct->persist($driver_info);
            $doct->flush();
            return $this->redirectToRoute('app_driver_index');
        } else {
            return $this->render('driver/new.html.twig', ['form' =>$form->createView()]);
        }
    }

    /** 
     * @Route("/driver/delete/{id}", name="app_driver_delete") 
     */ 
    public function deleteAction($id) { 
        $doct = $this->getDoctrine()->getManager(); 
        $driver = $doct->getRepository('AppBundle:Driver')->find($id); 
           
        if (!$driver) { 
            throw $this->createNotFoundException('No existe el driver con id: '.$id); 
        } 
        $doct->remove($driver); 
        $doct->flush(); 
        return $this->redirectToRoute('app_driver_index'); 
    }

    /** 
     * @Route("/driver/listnow/{id}", name="app_driver_list_day") 
     */ 
    public function listnowAction($id) {
        $doct = $this->getDoctrine()->getManager();
        $driver = $doct->getRepository('AppBundle:Driver')->find($id);
       
        if (!$driver) {
            throw $this->createNotFoundException('No existe el driver con id: '.$id); 
        }
        $order_query = 'SELECT * FROM order as o where driver_id = :driver_id and status = 0 and date_delivery = NOW();';
        
        $query = $em->getConnection()->prepare($order_query);
        // Set parameters 
        $query->bindValue('driver_id', $id);
        $query->execute();

        $orders = $statement->fetchAll();
        return $this->render('driver/orders.html.twig', ['driver' => $driver->getName(), 'orders' => $orders]);
    }
}