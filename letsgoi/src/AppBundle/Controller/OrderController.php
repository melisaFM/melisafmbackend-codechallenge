<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Order;
use AppBundle\Form\Type\OrderType;

class OrderController extends FOSRestController
{
    /**
     * @return array
     *
     * @View()
     * @Get("/orders")
     */
    public function getOrdersAction(){
        $orders = $this->getDoctrine()->getRepository("AppBundle:Order")
            ->findAll();
        if ($orders === null) {
          return new View("No se han encontrado pedidos.", Response::HTTP_NOT_FOUND);
        return array('order' => $orders);
    }

    /**
     * @param Order $order
     * @return array
     *
     * @View()
     * @ParamConverter("order", class="AppBundle:Order")
     * @Get("/order/{id}",)
     */
    public function getOrderAction(Order $order){
        return array('order' => $order);
    }

    /**
     * @var Request $request
     * @return View|array
     *
     * @View()
     * @Post("/order")
     */
    public function postOrderAction(Request $request)
    {
        $order = new Order();
        $form = $this->createForm(new OrderType(), $order);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            return array("order" => $order);
        }
        return array(
            'form' => $form,
        );
    }

    /**
     * Put action
     * @var Request $request
     * @var Order $order
     * @return array
     *
     * @View()
     * @ParamConverter("order", class="AppBundle:Order")
     * @Put("/order/{id}")
     */
    public function putOrderAction(Request $request, Order $order)
    {
        $form = $this->createForm(new OrderType(), $order);
        $form->submit($request);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            return array("order" => $order);
        }
        return array(
            'form' => $form,
        );
    }
    /**
     * Delete action
     * @var Order $order
     * @return array
     *
     * @View()
     * @ParamConverter("order", class="AppBundle:Order")
     * @Delete("/order/{id}")
     */
    public function deleteOrderAction(Order $order)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($order);
        $em->flush();
        return array("status" => "Deleted");
    }
}
