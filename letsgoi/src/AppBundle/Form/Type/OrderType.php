<?php

namespace AppBundle\\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;

class OrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('mail')
            ->add('phone')
            ->add('address')
            ->add('date_delivery')
            ->add('time_delivery')
            ->add('created')
            ->add('update')
            ->add('status');

        // Add driver_id
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();

            $option = array(
                    'class' => 'AppBundle\Entity\Driver',
                    'property' => 'id',
                    'query_builder' => function (EntityRepository $er) use ($driver) {
                        return $er->createQueryBuilder('d')
                            ->andWhere('d.status = 0')
                            ->addSelect('id')
                            ->setMaxResults(1)
                            ->orderBy('RAND()')
                            ->getOneOrNullResult()
                            ->execute();
                    },
                );

            if (!$option || null === $option->getId()) {
                $form->add('driver_id', 'entity', $option);
            }
        });    
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'data_class' => 'AppBundle\Entity\Order',
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "order";
    }


}