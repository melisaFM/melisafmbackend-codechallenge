<?php
// src/AppBundle/Entity/Order.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(
 *  name="`order`",
 *  indexes={
 *   @ORM\Index(name="driver_idx", columns={"driver_id"})
 * })
 * @UniqueEntity(
 *  fields={"mail"},
 *  message="This value must be unique"
 * )
 */
class Order{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
	private $name;

    /**
     * @ORM\Column(type="string", length=250)
     */
	private $surname;

    /**
     * @ORM\Column(type="string", length=150, unique=true)
     */
	private $mail;

    /**
     * @ORM\Column(type="string", length=15)
     */
	private $phone;

    /**
     * @ORM\Column(type="string", length=250)
     */
	private $address;

    /** 
    * @ORM\Column(type="datetime") 
    */	private $date_delivery;

    /**
     * @ORM\Column(type="string", length=25)
     */
	private $time_delivery;

    /**
     * @ORM\Column(type="integer", name="driver_id")
     */	
	private $driver_id;

    /** 
    * @ORM\Column(type="datetime") 
    */	private $created;

    /** 
    * @ORM\Column(type="datetime") 
    */
	private $update;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
	private $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Order
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Order
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Order
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Order
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Order
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set dateDelivery
     *
     * @param \DateTime $dateDelivery
     *
     * @return Order
     */
    public function setDateDelivery($dateDelivery)
    {
        $this->date_delivery = $dateDelivery;

        return $this;
    }

    /**
     * Get dateDelivery
     *
     * @return \DateTime
     */
    public function getDateDelivery()
    {
        return $this->date_delivery;
    }

    /**
     * Set timeDelivery
     *
     * @param string $timeDelivery
     *
     * @return Order
     */
    public function setTimeDelivery($timeDelivery)
    {
        $this->time_delivery = $timeDelivery;

        return $this;
    }

    /**
     * Get timeDelivery
     *
     * @return string
     */
    public function getTimeDelivery()
    {
        return $this->time_delivery;
    }

    /**
     * Set driverId
     *
     * @param integer $driverId
     *
     * @return Order
     */
    public function setDriverId($driverId)
    {
        $this->driver_id = $driverId;

        return $this;
    }

    /**
     * Get driverId
     *
     * @return integer
     */
    public function getDriverId()
    {
        return $this->driver_id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Order
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set update
     *
     * @param \DateTime $update
     *
     * @return Order
     */
    public function setUpdate($update)
    {
        $this->update = $update;

        return $this;
    }

    /**
     * Get update
     *
     * @return \DateTime
     */
    public function getUpdate()
    {
        return $this->update;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
